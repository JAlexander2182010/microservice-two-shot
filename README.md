# Wardrobify

Team:

* Jonathan Alexander - Hats
* James Lee - Shoes

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

The Hats models consist of two classes the LocationVO which contains the location name, section, and shelf.
The Hat model contains the name, fabric, color, picture and location. The poller checks for the location data
and is supplied to the wardrobe-api. All of the creation of the hats are stored in the http://localhost:8090/api/hats/
which contains the encoded location.
