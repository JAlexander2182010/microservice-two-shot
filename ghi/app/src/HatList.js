import React, {  useState } from "react";


function HatList(props) {
    const [wardrobe_name, setLocations] = useState('');
    const [hats, setHats] = useState(props.hats);

    if (!props.hats || props.hats.length === 0) {
        return <div>No hats found.</div>;
      }

    const deleteHat = (id) => {
      fetch(`http://localhost:8090/api/hats/${id}/`, {
        method: "DELETE",
      })};



    if (!props.hats || props.hats.length === 0) {
      return <div>No hats found.</div>;
    }

    return (
       <div>
          <h1>Hats</h1>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Style Name</th>
                <th>Fabric</th>
                <th>Color</th>
                <th>Picture URL</th>
                <th>Location</th>
              </tr>
            </thead>
            <tbody>
              {props.hats.map(hat => {

                return (
                  <tr key={hat.href}>
                    <td>{ hat.style_name }</td>
                    <td>{ hat.fabric }</td>
                    <td>{ hat.color }</td>
                    <td>{ hat.picture }</td>
                    <td>{ hat.wardrobe_location.closet_name }</td>
                    <button className="btn shadow p-2 mb-1 bg-info rounded" onClick={() => deleteHat(hat.id)}><em>Delete</em></button>
                  </tr>
                );
              })}
            </tbody>
          </table>
      </div>
    );
  }

  export default HatList;
