import React, { useState, useEffect } from 'react';

export default function HatForm(props){
        const [style_name, setStyleName] = useState('');
        const [color, setColor] = useState('');
        const [fabric, setFabric] = useState('');
        const [picture, setPicture] = useState('');
        const [wardrobe_location, setLocation] = useState('');
        const [wardrobe_locations, setLocations] = useState([]);

        useEffect(() => {
            async function getLocations() {
              const url = 'http://localhost:8100/api/locations/';
              const response = await fetch(url);
              if (response.ok) {
                const data = await response.json();
                setLocations(data.locations);
              }
            }
            getLocations();
          }, []);

        async function handleSubmit(event){
            event.preventDefault();
            const data = {
                style_name,
                color,
                fabric,
                picture,
                wardrobe_location,
            };


            const locationURL = `http://localhost:8090/api/hats/`;
            const fetchConfig = {
                method: "post",
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(locationURL, fetchConfig);
            console.log(response)
            if(response.ok){
                const newHat = await response.json();
                console.log(newHat)

                setStyleName('');
                setColor('');
                setFabric('');
                setPicture('');
                setLocation('');

            }
        }

        function handleStyleNameChange(event){
            const value = event.target.value;
            setStyleName(value);
        }

        function handleColorChange(event){
            const value = event.target.value;
            setColor(value);
        }

        function handleFabricChange(event){
            const value = event.target.value;
            setFabric(value);
        }

        function handlePictureChange(event){
            const value = event.target.value;
            setPicture(value);
        }

        function handleLocationChange(event){
            const value = event.target.value;
            setLocation(value);
        }


        return(
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                    <h1>Create a hat</h1>
                    <form onSubmit={handleSubmit} id="create-location-form">
                        <div className="form-floating mb-3">
                        <input value={style_name} onChange={handleStyleNameChange} placeholder="Style Name" required type="text" name="name" id="name" className="form-control" />
                        <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                        <input value={color} onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                        <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                        <input value={fabric} onChange={handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                        <label htmlFor="fabric">Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                        <input value={picture} onChange={handlePictureChange} placeholder="Picture" required type="text" name="picture" id="picture" className="form-control" />
                        <label htmlFor="picture">Picture</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleLocationChange} value={wardrobe_location} required name="wardrobe_location" id="wardrobe_location" className="form-select">
                                <option value="">Choose a location</option>
                                {wardrobe_locations.map(location => {
                                    return (
                                        <option key={location.id} value={location.href}>{location.closet_name}</option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                     </div>
                </div>
            </div>
            )


    }
