import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import HatList from './HatList';
import HatForm from './HatForm';
import Nav from './Nav';
import ShoesList from './ShoesList';
import { useState, useEffect } from 'react';



function App(props) {

  const [hats, setHats ] = useState([]);

  async function getHats() {
    const url = 'http://localhost:8090/api/hats/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setHats(data.hats);
    }
  }

  useEffect(() => {
    getHats();
  }, []);

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage shoes={props.shoes}/>} />
          <Route path="shoes" index element={<ShoesList shoes={props.shoes} />} />
          <Route path="hats">
            <Route index element={<HatList hats={hats}/>} />
          </Route>
          <Route path="hat_form">
            <Route index element={<HatForm hats={hats} />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
