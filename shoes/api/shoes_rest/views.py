from django.shortcuts import render
from django.http import JsonResponse
import json
from .models import Shoe, BinVO
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods

class BinListEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "import_href",
        "bin_number",
        "bin_size",
        "id",
    ]

class ShoesListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "name",
        "color",
        "manufacturer",
        "picture_url",
        "bin",
    ]
    encoders= {
        "bin": BinListEncoder(),
    }


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "name",
        "color",
        "picture_url",
        "bin",
    ]
    encoders = {
        "bin": BinListEncoder(),
    }



# Create your views here.
@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        print("Shoe ID!!!!")
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoesListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = content['bin']
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder = ShoeDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_shoe_detail(request, id):
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=id)
            return JsonResponse(
                {"shoe": shoe},
                encoder=ShoeDetailEncoder,
                safe=False
            )
        except Shoe.DoesNotExist:
            return JsonResponse(
                {"message": "This shoe doesn't exist."},
                status=400
            )
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Shoe.objects.filter(id=id).update(**content)
        shoe = Shoe.objects.get(id=id)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
