from django.shortcuts import render
from .models import Hat, LocationVO
from common.json import ModelEncoder
import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
        "section_number",
        "shelf_number",
    ]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "style_name",
        "picture",
        "fabric",
        "color",
        "wardrobe_location",
    ]
    encoders={
        "wardrobe_location":LocationVOEncoder(),
    }

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "fabric",
        "style_name",
        "color",
        "picture",
        "wardrobe_location",
    ]
    encoders={
        "wardrobe_location":LocationVOEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_hats(request):

    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)

        locations = LocationVO.objects.all()
        for location in locations:
            print(location.closet_name)

        try:
            # location_href = content["wardrobe_location"]
            wardrobe_location = LocationVO.objects.get(import_href=content["wardrobe_location"])
            content["wardrobe_location"] = wardrobe_location
            print(content)
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_hats(request, id):

    if request.method == "GET":
        hat = Hat.objects.get(id=id)

        return JsonResponse(
            {"hat": hat},
            encoder=HatDetailEncoder,
        )
    elif request.method == "DELETE":
        try:
            hat = Hat.objects.filter(id=id)
            count, _ = Hat.objects.filter(id=id).delete()
            return JsonResponse(
                {"deleted": count > 0}
            )
        except Hat.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
